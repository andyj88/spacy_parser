"""
TODO: write mod description
"""
import os
from spacy import util, load
from spacy.cli import download
from collections import defaultdict
from tqdm import tqdm


class Parser:
    """
    Parser class: parse a document into its sentences, tokens and various tags

    :param txt: text from a document to be parsed
    :type txt: str

    :param lang: language i.e. `en` for Englist
    :type lang: str 
    """

    def __init__(self, txt, lang='en'):
        self._lang = lang
        self._lang_model = self._load_lang_model()
        self._doc = self._process_text(txt)

    def _model_installed(self):
        """
        Check if language model is already installed
        """
        data_path = util.get_data_path()
        model_path = os.path.join(data_path, self._lang)        
        return os.path.exists(model_path)

    def _load_lang_model(self):
        """
        Load language model
        """
        if not self._model_installed():
            download(self._lang)
        return load(self._lang)

    
    def _process_text(self, txt):
        """
        Apply pipeline to txt
        """
        doc = self._lang_model.tokenizer(txt)

        for name, process in self._lang_model.pipeline:
            process(doc)
        assert doc.is_parsed, 'Document was not parsed'
        return doc
    
    def parse(self):
        """
        Parsers each sentence into the following parts:
            * Dependency tags: list, `dep_tags`
            * Lemmatised tags: list, `lemmas`
            * Named Entity Recognition tags: list, `ner_tags`
            * Part-of-speech tags: list, `pos_tags` and `pos_tags_2`
            * Text: str, `text`
            * Tokens: list, `words`
        """
        parsed_text = dict()
        for i, sent in enumerate(self._doc.sents):
            parts = defaultdict(list)
            text = sent.text
            parts['text'] = text
            for token in sent:
                parts['words'].append(str(token))
                parts['lemmas'].append(token.lemma_)
                parts['pos_tags_2'].append(token.tag_)
                parts['pos_tags'].append(token.pos_)
                parts['dep_tags'].append(token.dep_)
                parts['ner_tags'].append(token.ent_type_ if token.ent_type_ else 'O')
                parts['char_offsets'].append(token.idx)
                parts['abs_char_offsets'].append(token.idx)

            parts['char_offsets'] = [p - parts['char_offsets'][0] for p in parts['char_offsets']]
            parsed_text[i] = parts
        return parsed_text
