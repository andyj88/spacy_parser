import os
import sys
curr_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.split(curr_dir)[0]
mod_path = '{}/{}'.format(parent_dir, 'parser')
sys.path.append(mod_path)

from parser.parser_spacy import Parser


with open('{}/test.txt'.format(curr_dir), 'r') as f:
    txt = f.read()

parser = Parser(txt)
doc_parsed = parser.parse()
