# Parser w/ spaCy

Purpose of repo is to provide a functionality to make it obtain a parsed format of a text document.

Example:

```python
from parser.parser_spacy import Parser

doc_parsed = Parser(txt).parse()
```

The above returns the following information for the parsed document:

```json
{
    0: {
        'text': '...',
        'dep_tags': [...],  # Dependency tags
        'lemmas': [...],  # Lemmatised tags
        'ner_tags': [...],  # Named Entity Recognition tags
        'pos_tags': [...],  # Part-of-speech tags
        'pos_tags_2': [...],  # Part-of-speech tags
        'words': [...]  # Tokens
    },
    1: {},
    ...
    n_sentence: {}
}
```
